package ocr.codecrunch.co;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.ltapps.textscanner.R;

public class ResultActivity extends AppCompatActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_result);

		((TextView)findViewById(R.id.tv_result)).setText(getIntent().getStringExtra("data"));
	}
}
