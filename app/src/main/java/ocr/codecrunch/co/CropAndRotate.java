package ocr.codecrunch.co;

import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.SparseArray;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.vision.Frame;
import com.google.android.gms.vision.text.TextBlock;
import com.google.android.gms.vision.text.TextRecognizer;
import com.ltapps.textscanner.R;
import com.theartofdev.edmodo.cropper.CropImageView;


public class CropAndRotate extends AppCompatActivity implements View.OnClickListener, Toolbar.OnMenuItemClickListener {
	CropImageView cropImageView;
	private Toolbar toolbar;
	private FloatingActionButton mFab;
	private String message;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.crop_and_rotate);
		toolbar = (Toolbar) findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);
		ViewCompat.setElevation(toolbar, 10);
		toolbar.setOnMenuItemClickListener(this);
		Intent intent = getIntent();
		message = intent.getStringExtra(MainActivity.EXTRA_MESSAGE);
		cropImageView = (CropImageView) findViewById(R.id.cropImageView);

		cropImageView.setImageUriAsync(Uri.parse(message));
		mFab = (FloatingActionButton) findViewById(R.id.nextStep);
		mFab.setOnClickListener(this);

	}


	@Override
	public void onClick(View view) {
		if (view.getId() == R.id.nextStep) {
			cropImageView.setOnCropImageCompleteListener(new CropImageView.OnCropImageCompleteListener() {
				@Override
				public void onCropImageComplete(CropImageView view, CropImageView.CropResult result) {
					detectText(result.getBitmap());

				}
			});
			cropImageView.getCroppedImageAsync();
		}
	}

	public void detectText(Bitmap imageBitmap){

		if (imageBitmap != null) {

			TextRecognizer textRecognizer = new TextRecognizer.Builder(CropAndRotate.this).build();

			if (!textRecognizer.isOperational()) {
				Log.w("jinesh", "Detector dependencies are not yet available.");
				IntentFilter lowstorageFilter = new IntentFilter(Intent.ACTION_DEVICE_STORAGE_LOW);
				boolean hasLowStorage = registerReceiver(null, lowstorageFilter) != null;

				if (hasLowStorage) {
					Toast.makeText(CropAndRotate.this, "Low Storage", Toast.LENGTH_LONG).show();
					Log.w("jinesh", "Low Storage");
				}
			}


			Frame imageFrame = new Frame.Builder()
					.setBitmap(imageBitmap)
					.build();

			SparseArray<TextBlock> textBlocks = textRecognizer.detect(imageFrame);
			String data = "";

			for (int i = 0; i < textBlocks.size(); i++) {
				TextBlock textBlock = textBlocks.get(textBlocks.keyAt(i));
				data = data + textBlock.getValue();
			}
			Log.i("jinesh", data);

			startActivity(new Intent(getApplicationContext(),ResultActivity.class).putExtra("data",data));
		}

	}


	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.menu_rotate, menu);
		return super.onCreateOptionsMenu(menu);
	}


	@Override
	public boolean onMenuItemClick(MenuItem item) {
		switch (item.getItemId()) {
			case R.id.rotate_left:
				cropImageView.rotateImage(-90);
				break;
			case R.id.rotate_right:
				cropImageView.rotateImage(90);
				break;
		}
		return false;
	}
}
